#!/bin/bash

configured=true
mkdir -p /secrets


if [ -z ${G_USER+x} ]; then
  echo "WARNING: G_USER not set"
  configured=false
fi
echo "stage 1"
if [ -z ${G_PROJECT+x} ]; then
  echo "WARNING: G_PROJECT not set"
  configured=false
fi
echo "stage 2"
if [ -z ${G_CLUSTER+x} ]; then 
  echo "WARNING: G_CLUSTER not set"
  configured=false
fi
echo "stage 3"
if [ -z ${G_REGION+x} ]; then
  echo "WARNING: G_REGION not set"
  configured=false
fi
echo "stage 4"
if [ -z ${G_ZONE+x} ]; then
  echo "WARNING: G_ZONE not set"
  configured=false
fi
echo $GKE_KEYS | base64 -d > /secrets/service-account.json
echo "stage 6"

if [ "$configured" = true ]; then
  gcloud auth activate-service-account $G_USER --key-file $GOOGLE_APPLICATION_CREDENTIALS
  gcloud config set project $G_PROJECT
  gcloud config set container/cluster $G_CLUSTER
  gcloud config set compute/region $G_REGION
  gcloud config set compute/zone $G_ZONE
  export CLOUDSDK_CONTAINER_USE_V1_API_CLIENT=false && export CLOUDSDK_CONTAINER_USE_V1_API=false && gcloud beta container clusters get-credentials $G_CLUSTER --region $G_REGION --project $G_PROJECT
else
  echo "Not attempting to configure gcloud credentials"
fi
echo "y" | gcloud auth configure-docker
